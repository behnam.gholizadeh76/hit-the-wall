﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerHp : MonoBehaviour
{
    public GameObject ballPrefab;
    public int health = 3;
    public int ballx = 0;
    public int bally = 0;
    public Vector2 ballPosition;


    private void OnCollisionEnter2D(Collision2D other)
    {

        if (other.gameObject.tag == "ball")
        {

            Destroy(other.gameObject);
            if (health > 0)
            {
                StartCoroutine(ExecuteAfterTime(1));
                health--;
                ballPosition.Set(ballx, bally);
            }
            else if (health == 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                Time.timeScale = 1f;
            }
        }
    }

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSecondsRealtime(time);
        GameObject go = Instantiate(ballPrefab, ballPosition, Quaternion.identity);

        // Code to execute after the delay
    }
}
