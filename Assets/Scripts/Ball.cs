﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class Ball : MonoBehaviour
{
    private Rigidbody2D rb2d;

    public float maxSpeed = 10f;
    public int damage = 50;
    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();

    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        Wall enemy = other.gameObject.GetComponent<Wall>();
        if (enemy != null)
        {
            enemy.TakeDamage(damage);
        }
    }
    private void FixedUpdate()
    {
        rb2d.velocity = maxSpeed * (rb2d.velocity.normalized);
    }
}
