﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace UnityStandardAssets._2D
{
    public class Ship : MonoBehaviour
    {
        [SerializeField] private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.

        private Rigidbody2D m_Rigidbody2D;
        float horizontalMove = 0f;

        public int health = 3;

        void Update()
        {
            horizontalMove = Input.GetAxisRaw("Horizontal");
            if (GameObject.FindWithTag("Wall") == null)
            {
                SceneManager.LoadScene("Game Over");
            }
        }
        void FixedUpdate()
        {
            Move(horizontalMove);
        }
        private void Awake()
        {
            m_Rigidbody2D = GetComponent<Rigidbody2D>();

        }

        public void Move(float move)
        {

            m_Rigidbody2D.velocity = new Vector2(move * m_MaxSpeed, m_Rigidbody2D.velocity.x);


        }

        public void TakeDamage(int damage)
        {
            health -= damage;

            if (health <= 0)
            {
                Die();
            }
        }

        void Die()
        {
            Destroy(gameObject);
        }
    }
}
